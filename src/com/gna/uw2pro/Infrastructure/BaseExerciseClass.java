package com.gna.uw2pro.Infrastructure;

import com.gna.uw2pro.R;

import android.graphics.Typeface;
import android.graphics.drawable.StateListDrawable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class BaseExerciseClass extends CommonBaseClass {

	protected ImageButton leftArrowBtn;
	protected ImageButton infoBtn;
	protected TextView displayExercise1;
	protected TextView displayExercise2;
	protected TextView displayExercise3;
	protected TextView displayExercise4;
	protected TextView displayExercise5;
	protected TextView displayExercise6;
	protected TextView displayExercise7;
	protected TextView displayExercise8;
	protected ImageButton rightArrowBtn1;
	protected ImageButton rightArrowBtn2;
	protected ImageButton rightArrowBtn3;
	protected ImageButton rightArrowBtn4;
	protected ImageButton rightArrowBtn5;
	protected ImageButton rightArrowBtn6;
	protected ImageButton rightArrowBtn7;
	protected ImageButton rightArrowBtn8;

	ImageView titleImage;
	ImageView preExerCiseImage;
	LinearLayout display8parent;
	LinearLayout rightArrowBtn8Parent;

	public void intializeView() {
		leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowBtn);
		infoBtn = (ImageButton) findViewById(R.id.infoBtn);
		titleImage = (ImageView) findViewById(R.id.titleImage);
		preExerCiseImage = (ImageView) findViewById(R.id.preExerciseImage);
		display8parent = (LinearLayout) findViewById(R.id.displayExercise8Parent);
		rightArrowBtn8Parent = (LinearLayout) findViewById(R.id.rightArrowBtn8Parent);

		StateListDrawable infoBtnStates = new StateListDrawable();
		infoBtnStates.addState(new int[] { android.R.attr.state_pressed }, this
				.getResources().getDrawable(R.drawable.infobar_down));
		infoBtnStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.infobar_norm));
		infoBtn.setBackgroundDrawable(infoBtnStates);

		displayExercise1 = (TextView) findViewById(R.id.displayExercise1);
		displayExercise2 = (TextView) findViewById(R.id.displayExercise2);
		displayExercise3 = (TextView) findViewById(R.id.displayExercise3);
		displayExercise4 = (TextView) findViewById(R.id.displayExercise4);
		displayExercise5 = (TextView) findViewById(R.id.displayExercise5);
		displayExercise6 = (TextView) findViewById(R.id.displayExercise6);
		displayExercise7 = (TextView) findViewById(R.id.displayExercise7);
		displayExercise8 = (TextView) findViewById(R.id.displayExercise8);

		rightArrowBtn1 = (ImageButton) findViewById(R.id.rightArrowBtn1);
		rightArrowBtn2 = (ImageButton) findViewById(R.id.rightArrowBtn2);
		rightArrowBtn3 = (ImageButton) findViewById(R.id.rightArrowBtn3);
		rightArrowBtn4 = (ImageButton) findViewById(R.id.rightArrowBtn4);
		rightArrowBtn5 = (ImageButton) findViewById(R.id.rightArrowBtn5);
		rightArrowBtn6 = (ImageButton) findViewById(R.id.rightArrowBtn6);
		rightArrowBtn7 = (ImageButton) findViewById(R.id.rightArrowBtn7);
		rightArrowBtn8 = (ImageButton) findViewById(R.id.rightArrowBtn8);

		Typeface koratakiRgTypeFace = Typeface.createFromAsset(getAssets(),
				"fonts/KoratakiRg.ttf");
		displayExercise1.setTypeface(koratakiRgTypeFace);
		displayExercise2.setTypeface(koratakiRgTypeFace);
		displayExercise3.setTypeface(koratakiRgTypeFace);
		displayExercise4.setTypeface(koratakiRgTypeFace);
		displayExercise5.setTypeface(koratakiRgTypeFace);
		displayExercise6.setTypeface(koratakiRgTypeFace);
		displayExercise7.setTypeface(koratakiRgTypeFace);
		displayExercise8.setTypeface(koratakiRgTypeFace);

	}

	public void bindWorkoutA() {
		display8parent.setVisibility(View.GONE);
		rightArrowBtn8Parent.setVisibility(View.GONE);
		titleImage.setBackgroundResource(R.drawable.title01);
		preExerCiseImage.setBackgroundResource(R.drawable.preexercises_01);
		displayExercise1.setText("BW Squat Jumps");
		displayExercise2.setText("Knee Unders");
		displayExercise3.setText("Front Thruster");
		displayExercise4.setText("Bent Reverse DB Flye");
		displayExercise5.setText("Renegade Row");
		displayExercise6.setText("Sky Pointer Flyes");
		displayExercise7.setText("DB Finger Point Crunch");
	}

	public void bindWorkoutB() {
		titleImage.setBackgroundResource(R.drawable.title02);
		preExerCiseImage.setBackgroundResource(R.drawable.preexercises_02);
		displayExercise1.setText("Wide Pulldown");
		displayExercise2.setText("Incline 45 Curl");
		displayExercise3.setText("CG Push Up");
		displayExercise4.setText("Diagonal Alt Lunges");
		displayExercise5.setText("Burpees");
		displayExercise6.setText("180 Jump Squats");
		displayExercise7.setText("DB Lean");
		displayExercise8.setText("Knee Outs");
	}

	public void bindWorkoutC() {
		display8parent.setVisibility(View.GONE);
		rightArrowBtn8Parent.setVisibility(View.GONE);
		titleImage.setBackgroundResource(R.drawable.title03);
		preExerCiseImage.setBackgroundResource(R.drawable.preexercises_03);
		displayExercise1.setText("Push Up");
		displayExercise2.setText("Floor Superman");
		displayExercise3.setText("Pull Up");
		displayExercise4.setText("Burpee Knee Outs");
		displayExercise5.setText("Sky Pointers");
		displayExercise6.setText("Spiderman Walk outs");
		displayExercise7.setText("Split Frogs");

	}

	public void bindWorkoutD() {
		titleImage.setBackgroundResource(R.drawable.title04);
		preExerCiseImage.setBackgroundResource(R.drawable.preexercises_04);
		displayExercise1.setText("Db High Squat");
		displayExercise2.setText("Single Arm DB Press");
		displayExercise3.setText("Rev DB Lunges");
		displayExercise4.setText("Breakdancers");
		displayExercise5.setText("Lockdown Sky Pointers");
		displayExercise6.setText("Side DB Thrusters");
		displayExercise7.setText("Kickbacks");
		displayExercise8.setText("Plank");

	}

	public void bindWorkoutE() {
		titleImage.setBackgroundResource(R.drawable.title05);
		preExerCiseImage.setBackgroundResource(R.drawable.preexercises_05);
		displayExercise1.setText("CG DB Floor Press");
		displayExercise2.setText("Lunge & Curl");
		displayExercise3.setText("DB Put Ups");
		displayExercise4.setText("DB Jumping Jacks");
		displayExercise5.setText("Hammer Curls");
		displayExercise6.setText("Zig Zag Push Up");
		displayExercise7.setText("Total Body Power");
		displayExercise8.setText("Half & Half");

	}

}
