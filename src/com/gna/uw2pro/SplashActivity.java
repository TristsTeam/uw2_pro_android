package com.gna.uw2pro;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;

import com.gna.uw2pro.Infrastructure.KeepScreenOnBaseClass;

public class SplashActivity extends KeepScreenOnBaseClass implements Runnable {

	private int splashTime = 3000;

	// private static final String BASE64_PUBLIC_KEY =
	// "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAl5ttHb0aRCJ0ZaTW1InpE4umMgnFpm8lBoAjdEfQG1OUe3YbBSL5w6E5e6KppzjbVrJUOCn2g7whdSZAmI+L7uM1LoNogOcQr6r+8wtz66XZb1skcrfzNLzlf+dsjLtvYKt+tSYSSocHpODgG9JD2ZAIZ7ndw3iUv5f/3oocksscnS9TKk8kIYcHs2xAli9bnRhiXHcOOt4f4Vo5EG5jJ3s0XlTm99dNZMFy7/Js9KxbH6K00lHx0+qgwtfb8npcUctCoIO18f9PserNDOowWcy2NXm0wiK1l5XghLQuLAiP9MNz3WQzHrLMCzevhNkNDrb/ca/8LbRynzyC6dB7HQIDAQAB";
	// private LicenseCheckerCallback mLicenseCheckerCallback;
	// private LicenseChecker mChecker;

	/*
	 * private static final byte[] SALT = new byte[] { -46, 65, 30, -128, -103,
	 * -57, 74, -64, 51, 88, -95, -45, 77, -117, -36, -113, -11, 32, -64, 89 };
	 */

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		keepScreenOn();

		// Try to use more data here. ANDROID_ID is a single point of attack.
		// String deviceId = Secure.getString(getContentResolver(),
		// Secure.ANDROID_ID);
		//
		// // Library calls this when it's done.
		// mLicenseCheckerCallback = new MyLicenseCheckerCallback();
		// // Construct the LicenseChecker with a policy.
		// mChecker = new LicenseChecker(this, new ServerManagedPolicy(this,
		// new AESObfuscator(SALT, getPackageName(), deviceId)),
		// BASE64_PUBLIC_KEY);
		// if (!WWUCommon.getInstance(this).isVerify())
		// doCheck();
		// else {
		// Thread thread = new Thread(this);
		// thread.start();
		// }

		Thread thread = new Thread(this);
		thread.start();
	}

	Handler handle = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			Intent intent = new Intent();
			intent.setClass(SplashActivity.this, SecondSplashActivity.class);
			startActivity(intent);
			SplashActivity.this.finish();
		}
	};

	@Override
	public void run() {
		try {
			Thread.sleep(splashTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		handle.sendEmptyMessage(0);
	}

//	protected Dialog onCreateDialog(int id) {
//		final boolean bRetry = id == 1;
//		return new AlertDialog.Builder(this)
//				.setTitle(R.string.unlicensed_dialog_title)
//				.setMessage(
//						bRetry ? R.string.unlicensed_dialog_retry_body
//								: R.string.unlicensed_dialog_body)
//				.setPositiveButton(
//						bRetry ? R.string.retry_button : R.string.buy_button,
//						new DialogInterface.OnClickListener() {
//							boolean mRetry = bRetry;
//
//							public void onClick(DialogInterface dialog,
//									int which) {
//								if (mRetry) {
//									doCheck();
//								} else {
//									Intent marketIntent = new Intent(
//											Intent.ACTION_VIEW,
//											Uri.parse("http://market.android.com/details?id="
//													+ getPackageName()));
//									startActivity(marketIntent);
//								}
//							}
//						})
//				.setNegativeButton(R.string.quit_button,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								finish();
//							}
//						}).create();
//	}
//
//	private void doCheck() {
//		mChecker.checkAccess(mLicenseCheckerCallback);
//	}
//
//	private void displayDialog(final boolean showRetry) {
//		showDialog(showRetry ? 1 : 0);
//
//	}
//
//	private class MyLicenseCheckerCallback implements LicenseCheckerCallback {
//		public void allow(int policyReason) {
//			if (isFinishing()) {
//				// Don't update UI if Activity is finishing.
//				return;
//			}
//			Intent intent = new Intent();
//			intent.setClass(SplashActivity.this, SecondSplashActivity.class);
//			startActivity(intent);
//			SplashActivity.this.finish();
//		}
//
//		public void dontAllow(int policyReason) {
//			if (isFinishing()) {
//				// Don't update UI if Activity is finishing.
//				return;
//			}
//			Toast.makeText(SplashActivity.this, getString(R.string.dont_allow),
//					Toast.LENGTH_SHORT).show();
//		}
//
//		public void applicationError(int errorCode) {
//			if (isFinishing()) {
//				// Don't update UI if Activity is finishing.
//				return;
//			}
//			// This is a polite way of saying the developer made a mistake
//			// while setting up or calling the license checker library.
//			// Please examine the error code and fix the error.
//
//		}
//	}
//
//	@Override
//	protected void onDestroy() {
//		super.onDestroy();
//		mChecker.onDestroy();
//	}

}
